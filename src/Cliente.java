
import java.rmi.Naming;

/**
 * Classe CLiente que chama os métodos para execução remota
 * 
 * Autor: Diogo Reis Pavan
 * Ultima modificacao: 25/04/2018 22:20
 */

public class Cliente {

    private static final String NOME_SERVICO = "SERVIDOR_RMI";

    public Cliente(String h, String args) {

        System.out.println(executarTarefaRemota(h, args));
        System.out.println(executarTarefaRemota2(h, args));
        System.out.println(executarTarefaRemota3(h, args));
        System.out.println(executarTarefaRemota4(h, args));
    }//fim construtor	

    public static void main(String args[]) {

        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }
        new Cliente(args[0], args[1]);

    }//fim main

    public String executarTarefaRemota(String host, String argumentos) {

        //A tarefa foi executada com sucesso quando o retorno for "0" 
        String resultado = "1";

        IServidor servicoCliente = null;

        try {
            String caminhoServico = "rmi://" + host + "/" + NOME_SERVICO;

            //Obtem a referencia do servico remoto
            servicoCliente = (IServidor) Naming.lookup(caminhoServico);

            try {

                //Solicita a execucao remota do metodo 
                //que devera ser realizada pelo servico remoto
                resultado = servicoCliente.executarTarefa(argumentos);

            } catch (Exception e) {
                System.out.println("Excecao no cliente RMI:" + e.getMessage());
            }//fim catch

        } catch (Exception e) {
            System.out.println("Excecao ao obter a referencia remota do servico:" + e.getMessage());

        }//fim catch

        return resultado;

    }//fim executarTarefaRemota
    
    public String executarTarefaRemota2(String host, String argumentos) {

        //A tarefa foi executada com sucesso quando o retorno for "0" 
        String resultado = "";

        IServidor servicoCliente = null;

        try {
            String caminhoServico = "rmi://" + host + "/" + NOME_SERVICO;

            //Obtem a referencia do servico remoto
            servicoCliente = (IServidor) Naming.lookup(caminhoServico);

            try {

                //Solicita a execucao remota do metodo 
                //que devera ser realizada pelo servico remoto
                resultado = servicoCliente.executarTarefa2(argumentos);

            } catch (Exception e) {
                System.out.println("Excecao no cliente RMI:" + e.getMessage());
            }//fim catch

        } catch (Exception e) {
            System.out.println("Excecao ao obter a referencia remota do servico:" + e.getMessage());

        }//fim catch

        return resultado;

    }//fim executarTarefaRemota
    
    public String executarTarefaRemota3(String host, String argumentos) {

        //A tarefa foi executada com sucesso quando o retorno for "0" 
        String resultado = "";

        IServidor servicoCliente = null;

        try {
            String caminhoServico = "rmi://" + host + "/" + NOME_SERVICO;

            //Obtem a referencia do servico remoto
            servicoCliente = (IServidor) Naming.lookup(caminhoServico);

            try {

                //Solicita a execucao remota do metodo 
                //que devera ser realizada pelo servico remoto
                resultado = servicoCliente.executarTarefa3(argumentos);

            } catch (Exception e) {
                System.out.println("Excecao no cliente RMI:" + e.getMessage());
            }//fim catch

        } catch (Exception e) {
            System.out.println("Excecao ao obter a referencia remota do servico:" + e.getMessage());

        }//fim catch

        return resultado;

    }//fim executarTarefaRemota
    
    public String executarTarefaRemota4(String host, String argumentos) {

        //A tarefa foi executada com sucesso quando o retorno for "0" 
        String resultado = "";

        IServidor servicoCliente = null;

        try {
            String caminhoServico = "rmi://" + host + "/" + NOME_SERVICO;

            //Obtem a referencia do servico remoto
            servicoCliente = (IServidor) Naming.lookup(caminhoServico);

            try {

                //Solicita a execucao remota do metodo 
                //que devera ser realizada pelo servico remoto
                resultado = servicoCliente.executarTarefa4(argumentos);

            } catch (Exception e) {
                System.out.println("Excecao no cliente RMI:" + e.getMessage());
            }//fim catch

        } catch (Exception e) {
            System.out.println("Excecao ao obter a referencia remota do servico:" + e.getMessage());

        }//fim catch

        return resultado;

    }//fim executarTarefaRemota

}//fim classe
