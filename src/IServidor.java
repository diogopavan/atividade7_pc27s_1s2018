
import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Interface com métodos para execução remota
 * 
 * Autor: Diogo Reis Pavan
 * Ultima modificacao: 25/04/2018 22:20
 */
public interface IServidor extends Remote {

	public String executarTarefa(String argumentos) throws RemoteException;
        
	public String executarTarefa2(String argumentos) throws RemoteException;
        
        public String executarTarefa3(String argumentos) throws RemoteException;
        
        public String executarTarefa4(String argumentos) throws RemoteException;
}//fim interface
