
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/**
 * Classe com implementação dos métodos para execução remota
 * 
 * Autor: Diogo Reis Pavan
 * Ultima modificacao: 25/04/2018 22:22
 */
public class Servidor extends UnicastRemoteObject implements IServidor {

    private static final long serialVersionUID = 1L;
    private static final int PORTA_REGISTRO = 1099;
    private static final String NOME_SERVICO = "SERVIDOR_RMI";

    public Servidor() throws RemoteException {
        //Eh necessario ter um construtor aqui para
        //invocar o metodo construtor da superclasse
        super();
    }

    public static void main(String args[]) {

        //Gerenciador de Seguranca
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }

        try {
            //Criar o registry na porta especificada
            Registry registry = LocateRegistry.createRegistry(PORTA_REGISTRO);

            //Criar o servidor
            Servidor servidor = new Servidor();

            //Registrar o servidor
            registry.rebind(NOME_SERVICO, servidor);

            System.out.println("Servidor RMI em " + System.getenv("HOSTNAME") + " ativado.");

        } catch (Exception e) {

            System.out.println(e.getMessage());

        }//fim catch

    }//fim main

    public String executarTarefa(String args) throws RemoteException {

        String resultado = "";
        int resultadoIncrementado = Integer.parseInt(args) + 10;

        //Se o cliente fornecer como argumento a string 1
        if (args.equals("1")) {
            resultado = "Primeira opcao. Valor recebido do cliente: " + args + " Soma + 10: " + resultadoIncrementado;
        } else {
            resultado = "Segunda opcao. Valor recebido do cliente: " + args + " Soma + 10: " + resultadoIncrementado;
        }
        
        System.out.println("executarTarefa");

        return resultado;

    }//fim executarTarefa
    
    public String executarTarefa2(String args) throws RemoteException {
        
        String resultado = "";
        int num = Integer.parseInt(args);
        
        if (num%2 == 0){
            resultado = "Numero "+args+" eh par";
        } else {
            resultado = "Numero "+args+" eh impar";
        }
        System.out.println("executarTarefa2");
        
        return resultado;
    }
    
    public String executarTarefa3(String args) throws RemoteException {
        
        System.out.println("executarTarefa3");
        return "Comprimento do argumento: " + args.length();
    }
    
    public String executarTarefa4(String args) throws RemoteException {
        
        String resultado = "";

        //Se o cliente fornecer como argumento a string 1
        if (args.equals("200")) {
            resultado = "Voce adivinhou o numero que eu pensei!";
        } else {
            resultado = "Voce nao acertou o numero que eu pensei, tente novamente!";
        }
        System.out.println("executarTarefa4");

        return resultado;
    }

}//fim Servidor
